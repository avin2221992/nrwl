import { Component } from '@angular/core';

@Component({
  selector: 'nrwl-practise-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'school';
}

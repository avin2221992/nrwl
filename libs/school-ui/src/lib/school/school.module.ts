import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SchoolComponent } from './school/school.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SchoolComponent],
  exports:[SchoolComponent]
})
export class SchoolModule { }
